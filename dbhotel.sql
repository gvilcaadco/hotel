-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-08-2020 a las 23:52:44
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbhotel`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultar_pagos` (IN `pfecha_ini` DATE, IN `pfecha_fin` DATE)  begin
select pr.num_documento,concat(pr.apaterno,' ',pr.amaterno,' ',pr.nombre) as Cliente,
(select concat(apaterno,' ',nombre) from Persona where idpersona=r.idtrabajador) as Vendedor,
r.fecha_ingresa as Ingreso,r.fecha_salida as Salida,
(select numero from habitacion where idhabitacion=r.idhabitacion) as Habitacion,
p.tipo_comprobante as Comprobante,p.num_comprobante as Numero,p.total_pago as Pago
from reserva r inner join pago p on r.idreserva=p.idreserva
inner join persona pr on r.idcliente=pr.idpersona
where p.fecha_pago>=pfecha_ini and p.fecha_pago<=pfecha_fin;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `reporte_comprobante` (IN `pidreserva` INT)  begin
select p.idpago as IdVenta,p.tipo_comprobante as TipoDocumento,
(select concat(apaterno,' ',amaterno,' ',nombre) from persona where idpersona=r.idcliente)as Cliente,
(select concat(apaterno,' ',amaterno,' ',nombre) from persona where idpersona=r.idtrabajador)as Empleado,
p.num_comprobante as Serie,p.num_comprobante as Numero,
p.fecha_emision as Fecha,p.total_pago as TotalVenta,
'0' as Descuento,cast((p.total_pago-(p.total_pago/118*18)) as decimal(6,2)) as SubTotal,
cast((p.total_pago/118*18)as decimal(6,2)) as Igv,p.total_pago as TotalPagar,
r.estado as Estado,
(select numero from habitacion where idhabitacion=r.idhabitacion ) as Codigo,
'Reserva de Habitacion' as Producto,
datediff(r.fecha_salida,r.fecha_ingresa) as Cantidad,r.costo_alojamiento as PrecioVenta,
r.costo_alojamiento as Total
from pago p inner join reserva r on p.idreserva=r.idreserva
where r.idreserva=pidreserva
union
select p.idpago as IdVenta,p.tipo_comprobante as TipoDocumento,
(select concat(apaterno,' ',amaterno,' ',nombre) from persona where idpersona=r.idcliente)as Cliente,
(select concat(apaterno,' ',amaterno,' ',nombre) from persona where idpersona=r.idtrabajador)as Empleado,
p.num_comprobante as Serie,p.num_comprobante as Numero,
p.fecha_emision as Fecha,p.total_pago as TotalVenta,
'0' as Descuento,cast((p.total_pago-(p.total_pago/118*18)) as decimal(6,2)) as SubTotal,
cast((p.total_pago/118*18)as decimal(6,2)) as Igv,p.total_pago as TotalPagar,
r.estado as Estado,(select idproducto from producto where idproducto=c.idproducto) as Codito,
(select nombre from producto where idproducto=c.idproducto) as Producto,
c.cantidad as Cantidad,c.precio_venta as PrecioVenta,
(c.precio_venta*c.cantidad) as Total
from pago p inner join reserva r on p.idreserva=r.idreserva
inner join consumo c on r.idreserva=c.idreserva
where r.idreserva=pidreserva;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idpersona` int(11) NOT NULL,
  `codigo_cliente` varchar(50) NOT NULL,
  `destino` varchar(50) DEFAULT NULL,
  `estado_civil` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idpersona`, `codigo_cliente`, `destino`, `estado_civil`) VALUES
(3, 'VENDEDOR ', NULL, 'S'),
(9, 'OOOOOO', NULL, 'S'),
(10, 'OOOO', NULL, 'S'),
(11, '00000', NULL, 'S'),
(13, 'ESTIVADOR, SEGURIDAD', NULL, 'S'),
(14, 'OOOOOOOOOOOO', 'ninguno', 'S'),
(15, 'COMERCIANTE', NULL, 'S'),
(17, 'OOOOOOOO', NULL, 'S'),
(19, 'NEGOCIANTE', NULL, 'S'),
(21, 'tecnica', NULL, 'S');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consumo`
--

CREATE TABLE `consumo` (
  `idconsumo` int(11) NOT NULL,
  `idreserva` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` decimal(7,2) NOT NULL,
  `precio_venta` decimal(7,2) NOT NULL,
  `estado` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `consumo`
--

INSERT INTO `consumo` (`idconsumo`, `idreserva`, `idproducto`, `cantidad`, `precio_venta`, `estado`) VALUES
(2, 12, 18, '3.00', '2.00', 'Aceptado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `iddetalle_venta` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `idhabitacion` int(11) NOT NULL,
  `numero` varchar(4) NOT NULL,
  `piso` varchar(2) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `caracteristicas` varchar(512) DEFAULT NULL,
  `precio_diario` decimal(7,2) NOT NULL,
  `estado` varchar(15) NOT NULL,
  `tipo_habitacion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`idhabitacion`, `numero`, `piso`, `descripcion`, `caracteristicas`, `precio_diario`, `estado`, `tipo_habitacion`) VALUES
(2, '1', '1', 'cama simple, con tv cable, baño común', '1 sabana,1 sobre sabana,1 almohada y 1funda ', '15.00', 'Disponible', 'Individual'),
(3, '2', '1', 'cama simple, tv cable, baño comun', '1sabanas, 1sobre sabanas, 1almohada, 1funda', '15.00', 'Disponible', 'Individual'),
(4, '3', '1', 'cama simple, tv cable, baño comun', '1sabana, 1sobre sabana, 1funda', '15.00', 'Disponible', 'Individual'),
(5, '4', '2', 'cama simple, tv cable, baño comun', '1sabana, 1sobre sabana, 1almohada, 1funda', '15.00', 'Disponible', 'Individual'),
(6, '5', '2', 'cama simple, tv cable, baño comun', '1sabana, 1sobre sabana, 1funda', '15.00', 'Disponible', 'Individual'),
(7, '6', '2', 'cama simple, sin tv cable, baño comun', '1sabana, 1sobre sabana, 1almohada, 1funda', '10.00', 'Disponible', 'Individual'),
(8, '7', '2', 'cama matrimonial, tv cable, baño propio', '1sabana, 1sobre sabana, 2almohada, 2funda', '25.00', 'Disponible', 'Matrimonial'),
(9, '8', '2', 'cama matrimonial, tv cable, baño propio', '1sabana, 1sobre sabana, 2 almohadas, 2funda', '25.00', 'Disponible', 'Matrimonial'),
(10, '9', '2', '4camas simples,1camas matrimonial,  tv cable, baño propio', '5sabana, 5sobre sabana,6almohadas, 6fundas', '50.00', 'Disponible', 'Familiar'),
(11, '10', '2', '1cama matrimonial, 2camas simples, tv cable, baño propio ', '3sabana, 3sobre sabana, 4almohadas, 4 fundas', '45.00', 'Disponible', 'Familiar'),
(12, '11', '2', '1cama matrimonial, 1cama simple, tv cable, baño propio', '2sabana, 2sobre sabana, 3almohadas, 3fundas', '35.00', 'Disponible', 'Familiar'),
(13, '12', '3', 'cama matrimonial, tv cable, baño comun', '1sabana, 1sobre sabana, 2almohadas, 2fundas', '20.00', 'Disponible', 'Matrimonial'),
(14, '13', '3', 'cama simple, tv cable, baño comun', '1sabana, 1sobre sabana, 1almohada, 1funda', '18.00', 'Disponible', 'Individual'),
(15, '14', '3', 'cama matrimonial, tv cable, baño propio', '1sabana, 1sobre sabana, 2almohadas, 2fundas', '30.00', 'Disponible', 'Matrimonial'),
(16, '15', '3', '2camas simples, tv cable, baño comun', '2sabana, 2sobre sabanas, 2almohadas, 2fundas', '25.00', 'Disponible', 'Familiar'),
(17, '16', '3', 'cama matrimonial, tv cable, baño propio', '1sabana, 1sobre sabana, 2almohadas, 2fundas', '30.00', 'Disponible', 'Matrimonial'),
(18, '17', '3', 'cama simple, tv cable, baño propio', '1sabana, 1sobre sabana, 1almohada, 1funda', '25.00', 'Disponible', 'Individual'),
(19, '18', '3', 'cama simple, tv cable, baño propio', '1sabana, 1sobre sabana, 1almohada, 1funda', '25.00', 'Disponible', 'Individual');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `idpago` int(11) NOT NULL,
  `idreserva` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) NOT NULL,
  `num_comprobante` varchar(20) NOT NULL,
  `igv` decimal(4,2) NOT NULL,
  `total_pago` decimal(7,2) NOT NULL,
  `fecha_emision` date NOT NULL,
  `fecha_pago` date NOT NULL,
  `hora_pago` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`idpago`, `idreserva`, `tipo_comprobante`, `num_comprobante`, `igv`, `total_pago`, `fecha_emision`, `fecha_pago`, `hora_pago`) VALUES
(14, 12, 'Boleta', '1234', '18.00', '262.00', '2020-08-23', '2020-08-23', '04:49:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apaterno` varchar(20) NOT NULL,
  `amaterno` varchar(20) NOT NULL,
  `tipo_documento` varchar(15) NOT NULL,
  `num_documento` varchar(30) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `nombre`, `apaterno`, `amaterno`, `tipo_documento`, `num_documento`, `direccion`, `telefono`, `email`) VALUES
(1, 'Juan Josue', 'Huamansupa', 'Perez', 'DNI', '20970034', 'Lima-Lima Distrito El Agustino Mz T. LT 1. P joven El Agustino', '954478560', 'huamansupa@hotmail.com'),
(3, 'PAULINO R.', 'LANDEO', 'IRIARTE', 'DNI', '09222639', 'LOS LIRIOS MZ. R LT.9', '954999976', 'CHACLACAYO-LIMA'),
(7, 'ERICK ALEXANDER', 'LEON', 'COTRINA', 'DNI', '46712252', '', '', ''),
(9, 'ERICK ALEXANDER', 'LEON', 'COTRINA', 'DNI', '46712252', '00000', '000000', '000000'),
(10, 'JORGE', 'TOVAR', 'CAMPOS', 'DNI', '00152107', 'OOOOOOOOOOO', 'OOOOO', 'OOOO'),
(11, 'ARNULFO', 'TRINIDAD', 'DURAND', 'DNI', '80144237', 'LEONCIO PRADO CASERIO CORACORA', 'OOOO', 'HUANUCO'),
(13, 'RODOLFO ALEX', 'ESQUERRE', 'PEREZ', 'DNI', '80071918', 'SECTOR 2 GRP, 17 MZ K LT 23', '0000000', 'VILLA EL SALVADOR LIMA'),
(14, 'ALFREDO MILTON', 'BAUTISTA', 'AMES', 'DNI', '20101849', 'OOOOOOOOOOOOOOO', '00000000', 'OOOOOOOOOOOOOO'),
(15, ' NECYAS ', 'DEL VALLE', 'ROJAS', 'DNI', '00000000', 'OOOOOOOO', 'OOOOOOOO', 'LIMA'),
(17, 'JORGE ', 'CERRON', 'RAMOS', 'DNI', '73251865', 'OOOOOOOO', '000000000', 'OOOOOOOOO'),
(18, 'Nancy Luz', 'Navarro', 'Dominguez', 'DNI', '21014399', 'AGUSTO B..LEGUIA, Nªº735', '999718080', 'nancyluznav@hotmail.com'),
(19, 'NECIYAS ', 'VALLE', 'ROJAS', 'DNI', '10771703', 'aSSENTH EL GOLFO DE VENTANILLA MZ F LT 1 CALLE BELEN S/N', '966948501', 'CALLAO'),
(20, 'jose', 'veli', 'de la cruz', 'DNI', '45756286', 'satipo', '943126046', 'velisatipo@gmail.com'),
(21, 'Gina', 'Martinez', 'Gavilan', 'DNI', '45762385', 'Satipo', '8698532', 'gina@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `unidad_medida` varchar(20) NOT NULL,
  `precio_venta` decimal(7,2) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproducto`, `nombre`, `descripcion`, `unidad_medida`, `precio_venta`, `stock`) VALUES
(1, 'TOALLA HIGIENICA ', 'marca nosostras', 'UND', '1.00', 10),
(2, 'SPORADE', 'sporade', 'UND', '2.00', 10),
(3, 'SHAMPOO', 'sedal, h&s, pantene.', 'UND', '1.00', 10),
(4, 'PRESERVATIVO', 'preservativos gents', 'UND', '2.00', 10),
(5, 'POWER', 'power', 'UND', '3.00', 10),
(6, 'PILA AA', 'marca panasonic', 'UND', '1.00', 10),
(7, 'PILA AAA', 'pila AAA panasonic', 'UND', '1.00', 10),
(8, 'PAPEL HIGIENICO HOTELERO', 'marca disol', 'UND', '0.80', 10),
(9, 'PAPEL HIGIENICO SUAVE', 'marca suave', 'UND', '1.00', 10),
(10, 'PAÑAL DESCARTABLE ', 'marca huggies', 'UND', '1.00', 10),
(11, 'JABON DE TOCADOR', 'rexona , protex, Spa, lux, safeguard. neko', 'UND', '2.50', 10),
(12, 'JABON HOTELERO', 'marca neko', 'UND', '1.00', 10),
(13, 'ISOPO', 'cotton swabs', 'UND', '2.00', 10),
(14, 'INCA KOLA', 'inca kola 1/2 litro descartable', 'UND', '3.00', 10),
(15, 'GALLETAS', 'surtidas', 'UND', '1.00', 10),
(16, 'CREMA DENTAL', 'marca colgate', 'UND', '2.50', 10),
(17, 'COCA COLA ', '	coca cola 1/2 litro descartable', 'UND', '3.00', 10),
(18, 'CEPILLO', 'SMARTORAL, SAN-A', 'UND', '2.00', 7),
(19, 'AGUA MINERAL', 'SAN LUIS, SAN MATEO, CIELO, MANANTIAL', 'UND', '2.00', 10),
(20, 'AFEITADOR', 'DORCO, SCHIT', 'UND', '2.00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `idreserva` int(11) NOT NULL,
  `idhabitacion` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `idtrabajador` int(11) NOT NULL,
  `tipo_reserva` varchar(20) NOT NULL,
  `fecha_reserva` date NOT NULL,
  `fecha_ingresa` date NOT NULL,
  `hora_ingresa` varchar(15) DEFAULT NULL,
  `fecha_salida` date NOT NULL,
  `hora_salida` varchar(15) DEFAULT NULL,
  `costo_alojamiento` decimal(7,2) NOT NULL,
  `estado` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`idreserva`, `idhabitacion`, `idcliente`, `idtrabajador`, `tipo_reserva`, `fecha_reserva`, `fecha_ingresa`, `hora_ingresa`, `fecha_salida`, `hora_salida`, `costo_alojamiento`, `estado`) VALUES
(12, 2, 21, 1, 'Alquiler', '2020-08-04', '2020-08-04', '04:47:44', '2020-08-23', '04:49:13', '256.00', 'Pagada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `idpersona` int(11) NOT NULL,
  `sueldo` decimal(7,2) NOT NULL,
  `acceso` varchar(15) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`idpersona`, `sueldo`, `acceso`, `login`, `password`, `estado`) VALUES
(1, '750.00', 'Administrador', 'admin', 'admin', 'A'),
(18, '750.00', 'Digitador', 'digitador', '0505', 'A'),
(20, '2000.00', 'Administrador', 'jveli', 'jashyo', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `idtrabajador` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `impuesto` decimal(5,2) NOT NULL,
  `tipo_comprobante` varchar(20) DEFAULT NULL,
  `num_comprobante` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idventa`, `fecha`, `idtrabajador`, `idcliente`, `impuesto`, `tipo_comprobante`, `num_comprobante`) VALUES
(2, '2020-08-23', 1, 21, '18.00', 'Boleta', '1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idpersona`),
  ADD UNIQUE KEY `codigo_cliente_UNIQUE` (`codigo_cliente`);

--
-- Indices de la tabla `consumo`
--
ALTER TABLE `consumo`
  ADD PRIMARY KEY (`idconsumo`),
  ADD KEY `fk_consumo_producto_idx` (`idproducto`),
  ADD KEY `fk_consumo_reserva_idx` (`idreserva`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`iddetalle_venta`),
  ADD KEY `idventa` (`idventa`,`idproducto`),
  ADD KEY `idventa_2` (`idventa`),
  ADD KEY `fk_detalle_venta_producto` (`idproducto`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`idhabitacion`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`idpago`),
  ADD KEY `fk_pago_reserva_idx` (`idreserva`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `telefono_UNIQUE` (`telefono`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`idreserva`),
  ADD KEY `fk_reserva_habitacion_idx` (`idhabitacion`),
  ADD KEY `fk_reserva_cliente_idx` (`idcliente`),
  ADD KEY `fk_reserva_trabajador_idx` (`idtrabajador`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`idpersona`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`),
  ADD KEY `idcliente` (`idcliente`),
  ADD KEY `fk_venta_trabajador` (`idtrabajador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `consumo`
--
ALTER TABLE `consumo`
  MODIFY `idconsumo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `iddetalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `idhabitacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `idpago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `idreserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
