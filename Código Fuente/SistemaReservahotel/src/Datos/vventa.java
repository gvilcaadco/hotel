/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.Date;

/**
 *
 * @author JCarlos ArcilaD
 */
public class vventa {

    private int idventa;
    private Date fecha;
    private Double impuesto;  
    private int idcliente;
    private int idtrabajador;    
    private String tipo_comprobante;
    private String num_comprobante;
    
    public int getIdventa() {
        return idventa;
    }

    public void setIdventa(int idventa) {
        this.idventa = idventa;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Double impuesto) {
        this.impuesto = impuesto;
    }
    
    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public int getIdtrabajador() {
        return idtrabajador;
    }

    public void setIdtrabajador(int idtrabajador) {
        this.idtrabajador = idtrabajador;
    }
    public vventa() {
    }
    public String getTipo_comprobante() {
        return tipo_comprobante;
    }

    public void setTipo_comprobante(String tipo_comprobante) {
        this.tipo_comprobante = tipo_comprobante;
    }

    public String getNum_comprobante() {
        return num_comprobante;
    }

    public void setNum_comprobante(String num_comprobante) {
        this.num_comprobante = num_comprobante;
    }

    

    public vventa(int iventa,Date fecha,int idcliente,int idtrabajador,Double impuesto,String tipo_comprobante,String num_comprobante) {
        this.idventa=idventa;
        this.fecha=fecha;
        this.idcliente=idcliente;
        this.idtrabajador=idtrabajador;
        this.impuesto=impuesto;
        this.tipo_comprobante=tipo_comprobante;
        this.num_comprobante=num_comprobante;
    }
    
    
}
