/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.vreserva;
import Datos.vventa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author JCarlos ArcilaD
 */
public class fventa {
   private conexion mysql=new conexion();
   private Connection cn=mysql.conectar();
   private String SQL="";
   public Integer totalregistros;
   
   
   public DefaultTableModel mostrar(String buscar){
       DefaultTableModel modelo;
       
       String [] titulos = {"ID","idcliente","Cliente","idtrabajador","Trabajador","Fecha","Total","Comprobante","Número","Impuesto"};
       
       String [] registro =new String [10];
       
       totalregistros=0;
       modelo = new DefaultTableModel(null,titulos);
       
       SQL="select v.idventa,v.idcliente,"+
               "(select nombre from persona where idpersona=v.idcliente)as clienten,"+
               "(select apaterno from persona where idpersona=v.idcliente)as clienteap,"+
               "v.idtrabajador,(select nombre from persona where idpersona=v.idtrabajador)as trabajadorn,"+
               "(select apaterno from persona where idpersona=v.idtrabajador)as trabajadorap,"+
               "v.fecha,ifnull((select sum(cantidad*precio_venta) from detalle_venta where idventa=v.idventa),0) as total,"
               + "v.tipo_comprobante,v.num_comprobante,v.impuesto from venta v where (select apaterno from persona where idpersona=v.idcliente) like '%"+ buscar + "%' order by v.idventa desc limit 0,200";
       
       try {
           Statement st= cn.createStatement();
           ResultSet rs=st.executeQuery(SQL);
           
           while(rs.next()){
               registro [0]=rs.getString("idventa");
               registro [1]=rs.getString("idcliente");
               registro [2]=rs.getString("clienten") + " " + rs.getString("clienteap") ;
               registro [3]=rs.getString("idtrabajador");
               registro [4]=rs.getString("trabajadorn") + " " + rs.getString("trabajadorap");
               registro [5]=rs.getString("fecha");
               registro [6]=rs.getString("total");
               registro [7]=rs.getString("tipo_comprobante");
               registro [8]=rs.getString("num_comprobante");
               registro [9]=rs.getString("impuesto");
               
               totalregistros=totalregistros+1;
               modelo.addRow(registro);
               
           }
           return modelo;
           
       } catch (Exception e) {
           JOptionPane.showConfirmDialog(null, e);
           return null;
       }
      
       
   } 
   
   public boolean insertar (vventa dts){
       String fecha,hora;       
       fecha=funciones.getFechaActual();
       hora=funciones.getHoraActual();
       
       SQL="insert into venta (fecha,idtrabajador,idcliente,impuesto,tipo_comprobante,num_comprobante)" +
               "values (?,?,?,'18.00',?,?)";
       try {
           
           PreparedStatement pst=cn.prepareStatement(SQL);
           pst.setString(1, fecha);
           pst.setInt(2, dts.getIdtrabajador());
           pst.setInt(3, dts.getIdcliente());
           pst.setString(4, dts.getTipo_comprobante());
           pst.setString(5, dts.getNum_comprobante());
           
           int n=pst.executeUpdate();
           
           if (n!=0){
               return true;
           }
           else {
               return false;
           }
           
           
           
       } catch (Exception e) {
           JOptionPane.showConfirmDialog(null, e);
           return false;
       }
   }
   
   public boolean editar (vventa dts){
       SQL="update venta set idtrabajador=?,idcliente=?,tipo_comprobante=?,num_comprobante=?"+
               " where idventa=?";
           
       
       try {
           PreparedStatement pst=cn.prepareStatement(SQL);
           pst.setInt(1, dts.getIdtrabajador());
           pst.setInt(2, dts.getIdcliente());
           pst.setString(3, dts.getTipo_comprobante());
           pst.setString(4, dts.getNum_comprobante());
                    
           pst.setInt(5, dts.getIdventa());
           
           int n=pst.executeUpdate();
           
           if (n!=0){
               return true;
           }
           else {
               return false;
           }
           
       } catch (Exception e) {
           JOptionPane.showConfirmDialog(null, e);
           return false;
       }
   }
   
   
  
   public boolean eliminar (vventa dts){
       SQL="delete from venta where idventa=?";
       
       try {
           
           PreparedStatement pst=cn.prepareStatement(SQL);
           
           pst.setInt(1, dts.getIdventa());
           
           int n=pst.executeUpdate();
           
           if (n!=0){
               return true;
           }
           else {
               return false;
           }
           
       } catch (Exception e) {
           JOptionPane.showConfirmDialog(null, e);
           return false;
       } 
}
}
